<section class="home-slider owl-carousel">
        <div class="slider-item" style="background-image: url('assets/img/1.jpg');">

            <div class="container">
                <div class="row slider-text align-items-center">
                    <div class="col-md-12 col-sm-12 element-animate">
                        
                        <h4>
                            درمانگاه های تخصصی و فوق تخصصی بیمارستان دکتر شریعتی
                        </h4>
                    </div>
                </div>
            </div>

        </div>

        <div class="slider-item" style="background-image: url('assets/img/3.jpg');">
            <div class="container">
                <div class="row slider-text align-items-center">
                    <div class="col-md-12 col-sm-12 element-animate">
                        
                        <h4>
                            درمانگاه های تخصصی و فوق تخصصی بیمارستان دکتر شریعتی
                        </h4>
                    </div>
                </div>
            </div>

        </div>
        <div class="slider-item" style="background-image: url('assets/img/2.jpg');">
            <div class="container">
                <div class="row slider-text align-items-center">
                    <div class="col-md-12 col-sm-12 element-animate animate1">
                        
                        <h4>
                            درمانگاه های تخصصی و فوق تخصصی بیمارستان دکتر شریعتی
                        </h4>
                    </div>
                </div>
            </div>

        </div>
    </section>