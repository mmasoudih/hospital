<?php
session_start();
ob_start();
error_reporting(E_ALL);
ini_set('display_errors', 1);
define("SITE_NAME","پنل مدیریت سامانه نوبت دهی");
date_default_timezone_set("Asia/Tehran");
require_once "jdf.php";

function connectDB(){
    $servername = "localhost"; // database hostname
    $username = "root"; //database username
    $password = ""; //database password
    $db = "hospital"; // database name
    // Create connection
    $conn = mysqli_connect($servername, $username, $password, $db);

    // Check connection
    if (!$conn) {
        die("Connection failed: " . mysqli_connect_error());
    }
    mysqli_query($conn,"SET NAMES UTF8");
    return $conn;
}

?>