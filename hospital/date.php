<?php
 include_once 'functions/patient.php';

?>
<!DOCTYPE html>
<html><head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
 
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <meta name="keywords" content="بیمارستان شریعتی, بیمارستان شریعتی تهران, نوبت دهی شریعتی, نوبت دهی اینترنتی شریعتی,نوبت دهی اینترنتی درمانگاه های بیمارستان شریعتی">
    <title>نوبت دهی اینترنتی درمانگاه های بیمارستان شریعتی</title>
    
    <link href="assets/css/css.css" rel="stylesheet">
    <link href="assets/css/styles.css" rel="stylesheet">

     <link href="assets/fonts/css/all.css" rel="stylesheet">

</head>
<body>
    <header>
        <div class="container header0">
            <div class="row">
                <div class="col col-lg-11 text-right">
                    
                    
                    
                </div>
                <div class="col col-lg-1 text-left">
                    <img src="assets/css/logo.png">
                </div>
            </div>
        </div>
        <!-- <div class="container-fluid header_gradient">
         </div> -->
    </header>
     <section id="body">
        <div class="container-fluid all-page ">
            <div class="container-fluid main-body-top" style="min-height: 426px;">
                <div class="container-fluid header1">
                    <div class="container header2">
                        <div class="row">
                            <div class="col col-lg-8">
                                
                                <div class="col col-lg-12 text-right">
                                    <h4 class="title_">درمانگاه های تخصصی و فوق تخصصی بیمارستان دکتر شریعتی</h4>
                                </div>
                            </div>
                            <div class="col col-lg-4 text-left">
                                <a href="index.php" style="margin-top: 18px" class="btn btn-white ">صفحه  نخست</a>
                                <a href="#" style="margin-top: 18px" class="btn btn-white back">صفحه قبل</a>
                            </div>
                        </div>
                    </div>
                </div>
                  



<ul class="breadcrumb">
    <li class="active">
        <a href="#">انتخاب نوع درمانگاه  </a> 
    </li>        
    <?php if(isset($_GET['type'])): ?>                              
    <li class="active">
        <a href="#">انتخاب بیمارستان  </a> 
    </li>  
    <?php  endif; if(isset($_GET['child'])): ?>
    <li class="active">
        <a href="#">انتخاب پزشک  </a> 
    </li>  
    <?php  endif; if(isset($_GET['doctor_id'])): ?>
    <li class="active">
        <a href="#">انتخاب روز   </a> 
    </li>  
    <?php endif; ?>
</ul>


<div class="container-fluid main-body_" style="min-height: 426px;">
    <div class="container content text-center">

        <div class="seprator">
            <span class="glyphicon glyphicon-star sep-glyp1"></span>
            <span class="glyphicon glyphicon-star sep-glyp2"></span>
            <span class="glyphicon glyphicon-star sep-glyp3"></span>
            <h3>لطفا جهت رزرو نوبت نوع درمانگاه خود را انتخاب کنید.</h3>
            <div class="course-div-sep1">
                <div class="course-div-sep2"></div>
            </div>
        </div>
        <div class="row">
        <?php

        require_once './include/jdf.php';
        $date['today'] = fa2en(jdate('w'));

        for ($i = 0; $i < $date['today']; $i++) {
            $date [$i] = date('Y-m-d', strtotime(" +{$i} day"));
            $date [$i] = persianDate($date[$i]);
            // echo ($date[$i] . '-> ' . $i . '<br>');
        }
        for ($i = $date['today']; $i < 7; $i++) {
            $date [$i] = date('Y-m-d', strtotime(" +{$i} day"));
            $date[$i] = persianDate($date[$i]);
            // echo ($date[$i] . '-> ' . $i . '<br>');
        }
        
        $row = select_date_by_doctor_id($_GET['doctor_id']);
        // $q = explode(",","2,3,5,6");
        
            $day = NULL;
            $time = NULL;
            while($data = mysqli_fetch_assoc($row)):
                
                $a = explode(",",$data['date']); 
                      foreach($a as $id):

        ?>
        
            
            <div class="col-lg-4">
                <?
                    // echo $date[$id];
                    echo "
                        <a
                            href='form.php?type={$_GET['type']}&child={$_GET['child']}&doctor_id={$_GET['doctor_id']}&date={$id}&time={$date[$id]}'
                            class='btn btn-info btn-lg btn-sections'
                            style='min-height: 160px'
                        >
                            " . show_day_by_id($id) . "
                            <h6> " . show_hour_by_id($id) . " </h6>
                            <h6> " . $date[$id] . " </h6>
                        </a>
                    ";
                ?>
                <!-- <a href="form.php?type=<?php echo $_GET['type']; ?>&child=<?php echo $_GET['child']; ?>&doctor_id=<?php echo $_GET['doctor_id']; ?>&date=<?php echo $id; ?>" class="btn btn-info btn-lg btn-sections" style="min-height: 160px">
                    <?php echo show_day_by_id($id); ?>
                    <h6> ساعت پذیرش: <?php echo show_hour_by_id($id); ?></h6>                
                </a> -->
            </div>
        
        <?php
            
           
            
            endforeach;
        endwhile;
        ?>
        


        
    </div>
</div>
            <div class="container-fluid footer">
                <div class="container">
                    <h4 class="company">
                        
                    </h4>
                    <p>ساخته شده توسط : سحر تیموری</p>
                </div>
            </div>
        </div>

    </section>
    <!-- <script src="assets/js/jquery.js"></script> -->

    <!-- <script src="assets/js/bootstrap.js"></script> -->

    
    <script>
        var WindowHeight = $(document).height() - 1;
        var HeaderHeight = $('.header0').innerHeight() + $('.header_gradient').innerHeight() + $('.header1').innerHeight();
        var footerHeight = $('.footer').innerHeight();
        var bodyHeight = WindowHeight - HeaderHeight - footerHeight;
        $('.main-body_').css('min-height', bodyHeight);
        $('.main-body-top').css('min-height', bodyHeight);
        $('.back').click(function () {
            history.back();
            return false;
           // history.go(-1)
        })
    </script>



</body></html>