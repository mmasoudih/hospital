<!DOCTYPE html>
<html><head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
     <title>نوبت دهی اینترنتی درمانگاه های بیمارستان شریعتی</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="hospital_files/css.css" rel="stylesheet">
    <!-- Theme Style -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <link href="assets/css/animate.css" rel="stylesheet">
    <link href="assets/css/owl.carousel.min.css" rel="stylesheet">
    <link href="assets/css/bootstrap-datepicker.css" rel="stylesheet">
    <link href="assets/css/jquery.timepicker.css" rel="stylesheet">
    <link href="assets/css/style.css" rel="stylesheet">

</head>
<body>
<header role="banner">

<div class="top-bar">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-6 col-5">
                <img src="assets/img/tums.png">

                <a href="#" style="cursor:context-menu" data-toggle="modal" data-target="#modalAppointment"><label>(021)</label> 88027412 - راهنمای تلفنی نوبت‌دهی: 88004017 </a>

            </div>
            <div class="col-md-6 col-sm-6 col-7 text-right">

                <a href="#" style="cursor:context-menu" data-toggle="modal" data-target="#modalAppointment">سیستم نوبت دهی اینترنتی</a>

                <img src="assets/img/shariati.png">

            </div>
        </div>
    </div>
</div>
</header>
