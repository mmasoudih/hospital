
<?php
include_once 'functions/patient.php';
$parent = $_GET['type'];
$hospital = $_GET['child'];
$doctor = $_GET['doctor_id'];
$date = $_GET['date'];
$res = show_doctor_by_hospital_id($hospital);
$row = mysqli_fetch_assoc($res);
$hospital_res = show_hospital_name_by_id($hospital);
$hospital_row = mysqli_fetch_assoc($hospital_res);
$parent_res = show_parent_name_by_id($parent);
$parent_row = mysqli_fetch_assoc($parent_res);
$date_res = show_date_name_by_id($date);
$date_row = mysqli_fetch_assoc($date_res);

$doctor_name = $row['name']." ".$row['lastname'];
$hospital_name = $hospital_row['name'];
$parent_name = $parent_row['name'];
$day = $date_row['id'];
$_SESSION['doctor_name'] = $doctor_name;
$_SESSION['hospital_name'] = $hospital_name;
$_SESSION['parent_name'] = $parent_name;
$_SESSION['day'] = $day;


if(isset($_POST['btn'])){
    $data = $_POST['frm'];
    $_SESSION['code'] = $data['code']; 
    $time = $_GET['date'];
    $doctor_id = $_GET['doctor_id'];
    $hospital_id = $_GET['type'];
    add_patient($data,$time,$doctor_id,$hospital_id);
}
?>
<!DOCTYPE html>
<html><head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
 
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <meta name="keywords" content="بیمارستان شریعتی, بیمارستان شریعتی تهران, نوبت دهی شریعتی, نوبت دهی اینترنتی شریعتی,نوبت دهی اینترنتی درمانگاه های بیمارستان شریعتی">
    <title>نوبت دهی اینترنتی درمانگاه های بیمارستان شریعتی</title>
    
    <link href="assets/css/css.css" rel="stylesheet">
    <link href="assets/css/styles.css" rel="stylesheet">

     <link href="assets/fonts/css/all.css" rel="stylesheet">
     <script src="admin/js/city.js"></script>

</head>
<body>
    <header>
        <div class="container header0">
            <div class="row">
                <div class="col col-lg-11 text-right">
                    
                    
                    
                </div>
                <div class="col col-lg-1 text-left">
                    <img src="assets/css/logo.png">
                </div>
            </div>
        </div>
        <!-- <div class="container-fluid header_gradient">
         </div> -->
    </header>
     <section id="body">
        <div class="container-fluid all-page ">
            <div class="container-fluid main-body-top" style="min-height: 426px;">
                <div class="container-fluid header1">
                    <div class="container header2">
                        <div class="row">
                            <div class="col col-lg-8">
                                
                                <div class="col col-lg-12 text-right">
                                    <h4 class="title_">درمانگاه های تخصصی و فوق تخصصی بیمارستان دکتر شریعتی</h4>
                                </div>
                            </div>
                            <div class="col col-lg-4 text-left">
                                <a href="index.php" style="margin-top: 18px" class="btn btn-white ">صفحه  نخست</a>
                                <a href="#" style="margin-top: 18px" class="btn btn-white back">صفحه قبل</a>
                            </div>
                        </div>
                    </div>
                </div>
                  



    <ul class="breadcrumb">
                                    </ul>




<div class="container-fluid main-body_" style="min-height: 426px;">
    <div class="container content text-center">

        
        <div class="row">

    <form action="final.php?type=<?php echo $parent ?>&child=<?php echo $hospital; ?>&doctor_id=<?php echo $doctor; ?>&date=<?php echo $date; ?>&time=<?php echo $_GET['time'] ?>" method="post">


       


        <div class="container-fluid main-body_" style="min-height: 968px;">
            <div class="container content text-center">
                <div class="alert alert-danger text-right">
                    <strong>توجه !</strong> تا زمانی که برگه پرینت نوبت به شما نمایش داده نشد ، نوبت شما ثبت نهایی نشده است
                    <p>ممکن است نوبت شما به یکی از دلایل زیر نهایی نشده باشد
        (برگه پریبنت نمایش داده نشود ) در این صورت دکمه دریافت برگه نوبت غیر 
        فعال نمیگردد</p>
                    <p>1- نوبت به اتمام رسیده باشد</p>
                    <p>2- یکی از فیلد های الزامی پر نشده باشد.</p>
                </div>
                

                <div class="seprator">
                    <span class="glyphicon glyphicon-star sep-glyp1"></span>
                    <span class="glyphicon glyphicon-star sep-glyp2"></span>
                    <span class="glyphicon glyphicon-star sep-glyp3"></span>
                    
                    <div class="course-div-sep1">
                        <div class="course-div-sep2"></div>
                    </div>
                </div>
                <div class="row">
                    
                    <div class="col-lg-12 text-right">



                        <div class="form-group form-group-black wow bounceInRight animated">
                            <label class="control-label" for="inputLarge"> <span class="text-danger">(فیلد های ستاره دار الزامی هستند.)</span></label>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group form-group-black">
                                    <label class="control-label" for="inputLarge">کد ملی : <span class="text-danger">*</span></label>
                                    <input class="form-control force onlynumber english-input" id="ppIdTurn" maxlength="10" name="frm[code]" placeholder="کد ملی را در این قسمت وارد کنید" type="text" required oninput="this.setCustomValidity('')" oninvalid="this.setCustomValidity('کد ملی خود را وارد کنید')">
                                    <span class="material-input"></span>
                                </div>
                            </div>

            

                                
                        
                            <div class="col-lg-6">
                                <div class="form-group form-group-black">
                                    <label class="control-label" for="inputLarge">نام بیمار : <span class="text-danger">*</span></label>
                                    <input class="form-control capslock force minlenght  " data-val="true" data-val-required="تکمیل  فیلد الزامی است." id="pPatientTurn" minlenght="3" name="frm[name]" placeholder="" type="text" required oninput="this.setCustomValidity('')" oninvalid="this.setCustomValidity('نام خود را وارد کنید')">
                                    <span class="material-input"></span>
                                </div>
                            </div>   
                            <div class="col-lg-6">
                                <div class="form-group form-group-black">
                                    <label class="control-label" for="inputLarge">نام خانوادگی : <span class="text-danger">*</span></label>
                                    <input class="form-control force capslock minlenght" data-val="true" data-val-required="تکمیل  فیلد الزامی است." id="pFNameTurn" minlenght="3" name="frm[lastname]" placeholder="" type="text" required oninput="this.setCustomValidity('')" oninvalid="this.setCustomValidity('نام خانوادگی را وارد کنید')">
                                    <span class="material-input"></span>
                                </div>
                                </div>
                            <div class="col-lg-6">
                                <div class="form-group form-group-black">
                                    <label class="control-label" for="inputLarge">نام پدر : <span class="text-danger">*</span></label>
                                    <input class="form-control force capslock minlenght" data-val="true" data-val-required="The ppFatherName field is required." id="pFaNameTurn" minlenght="3" name="frm[fathername]" placeholder="" type="text" required oninput="this.setCustomValidity('')" oninvalid="this.setCustomValidity('نام پدر را وارد کنید')">
                                    <span class="material-input"></span>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group form-group-black">
                                    <label class="control-label" for="inputLarge"> شماره تلفن همراه : <span class="text-danger">*</span><span class="text-warning alert-msg">شامل 10 رقم -  </span><span class="text-warning alert-msg">*********9 </span></label>
                                    <input class="form-control force onlynumber maxlength minlenght english-input" data-val="true" data-val-required="The ppHomeMbl field is required." id="pPhoneTurn" maxlength="10" minlenght="10" name="frm[tel]" placeholder="" type="text" required oninput="this.setCustomValidity('')" oninvalid="this.setCustomValidity('تلفن خود را وارد کنید')">
                                    <span class="material-input"></span>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group form-group-black">
                                    <label class="control-label" for="inputLarge">شهر : <span class="text-danger ">*</span></label>
                                    <select name="frm[city]" id="ppCityId" class="form-control force" onChange="iranwebsv(this.value);getStateName(this);" required oninput="this.setCustomValidity('')" oninvalid="this.setCustomValidity('استان را انتخاب کنید')">
                                        <option value="">لطفا استان را انتخاب نمایید</option>
                                        <option value="1">تهران</option>
                                        <option value="2">گیلان</option>
                                        <option value="3">آذربایجان شرقی</option>
                                        <option value="4">خوزستان</option>
                                        <option value="5">فارس</option>
                                        <option value="6">اصفهان</option>
                                        <option value="7">خراسان رضوی</option>
                                        <option value="8">قزوین</option>
                                        <option value="9">سمنان</option>
                                        <option value="10">قم</option>
                                        <option value="11">مرکزی</option>
                                        <option value="12">زنجان</option>
                                        <option value="13">مازندران</option>
                                        <option value="14">گلستان</option>
                                        <option value="15">اردبیل</option>
                                        <option value="16">آذربایجان غربی</option>
                                        <option value="17">همدان</option>
                                        <option value="18">کردستان</option>
                                        <option value="19">کرمانشاه</option>
                                        <option value="20">لرستان</option>
                                        <option value="21">بوشهر</option>
                                        <option value="22">کرمان</option>
                                        <option value="23">هرمزگان</option>
                                        <option value="24">چهارمحال و بختیاری</option>
                                        <option value="25">یزد</option>
                                        <option value="26">سیستان و بلوچستان</option>
                                        <option value="27">ایلام</option>
                                        <option value="28">کهگلویه و بویراحمد</option>
                                        <option value="29">خراسان شمالی</option>
                                        <option value="30">خراسان جنوبی</option>
                                        <option value="31">البرز</option>
                                    </select>
                                    
                                </div>
                                <div class="form-group form-group-black">
                                    <select name="city" class="form-control m-bot15 p" id="city" onChange="getCityName(this);" required oninput="this.setCustomValidity('')" oninvalid="this.setCustomValidity('شهر را انتخاب کنید')">
                                        <option value="">لطفا استان را انتخاب نمایید</option>
                                    </select>
                                </div>
                                <input id="state" type = "hidden" name = "frm[state_name]" value = "" />
                                <input id="city_name" type = "hidden" name = "frm[city_name]" value = "" />
                            </div>
                            <div class="col-lg-6">
                                <label class="control-label" for="inputLarge">آدرس : <span class="text-danger">شامل حداقل 20 کارکتر *</span></label>
                                <input class="form-control force capslock  minlenght" data-val="true" data-val-required="The ppHomeAdr field is required." id="ppHomeAdr" minlenght="3" name="frm[address]" placeholder="" type="text" required oninput="this.setCustomValidity('')" oninvalid="this.setCustomValidity('آدرس را وارد کنید')">
                                <span class="material-input"></span>
                            </div>
                        </div>
                        

                        
                        
                        <div class="form-group form-group-material-light-blue-500 text-left">
                            <button type="submit" class="sub_validation btn btn-info  btn-lg" name="btn">دریافت برگه نوبت  </button>
                            <a href="index.php">
                                <button type="button" class="btn btn-danger btn-lg ">انصراف</button>
                            </a>
                        </div>

                        <div class="form-group form-group-black">
                            <label class="control-label" for="inputLarge">با توجه به الزام ورود کد ملی جهت رزرو نوبت ، مراجعه کنندگان اتباع خارجی به صورت حضوری به درمانگاه مراجعه نمایند.</label>
                        </div>

                    </div>
                </div>
                <div class="loading" style="position:absolute;top:30%;right:45% ;display: none">
                    <img src="start_5_files/loading_img.gif">
                    <br>
                    <span>لطفا منتظر بمانید ...</span>
                </div>

            </div>
        </div> 
        </form>


        </div>
    </div>
</div>
            <div class="container-fluid footer">
                <div class="container">
                    <h4 class="company">
                        
                    </h4>
                    <p>ساخته شده توسط : سحر تیموری</p>
                </div>
            </div>
        </div>

    </section>
    <!-- <script src="assets/js/jquery.js"></script> -->

    <!-- <script src="assets/js/bootstrap.js"></script> -->

    
    <!-- <script>
        var WindowHeight = $(document).height() - 1;
        var HeaderHeight = $('.header0').innerHeight() + $('.header_gradient').innerHeight() + $('.header1').innerHeight();
        var footerHeight = $('.footer').innerHeight();
        var bodyHeight = WindowHeight - HeaderHeight - footerHeight;
        $('.main-body_').css('min-height', bodyHeight);
        $('.main-body-top').css('min-height', bodyHeight);
        $('.back').click(function () {
            history.back();
            return false;
           // history.go(-1)
        })
    </script> -->



</body></html>