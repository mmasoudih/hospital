<?php
require_once 'include/config.php';
function show_parent_name_by_id($id){
    $conn = connectDB();
    $sql = "SELECT * FROM `hospital` WHERE `id`=$id";
    $result = mysqli_query($conn,$sql);
    return $result;    
}
function show_parent_type(){
    $conn = connectDB();
    $sql = "SELECT * FROM `hospital` WHERE `parent_id`=0";
    $result = mysqli_query($conn,$sql);
    return $result;    
}
function show_child_by_id($id){
    $conn = connectDB();
    $sql = "SELECT * FROM `hospital` WHERE `parent_id`=$id";
    $result = mysqli_query($conn,$sql);
    return $result;  
}
function show_doctor_by_hospital_id($id){
    $conn = connectDB();
    $sql = "SELECT * FROM `doctor` WHERE `hospital_id`=$id";
    $result = mysqli_query($conn,$sql);
    return $result; 
}
function show_hospital_name_by_id($id){
    $conn = connectDB();
    $sql = "SELECT (`name`) FROM `hospital` WHERE `id`=$id";
    $result = mysqli_query($conn,$sql);
    return $result; 
}
function select_date_by_doctor_id($id){
    $conn = connectDB();
    
    $sql = "SELECT d.date ,t.id,t.day,t.hour FROM time as t, doctor as d WHERE d.date = t.id and  d.id=$id";
    $result = mysqli_query($conn,$sql);
    return $result; 
}
function showTimeList_patient(){
    $conn = connectDB();
    $sql = "SELECT * FROM `time`";
    $result = mysqli_query($conn,$sql);
    return $result;
}
function get_count($date){
    $conn = connectDB();
    $sql = "SELECT COUNT(`id`) FROM `patient` WHERE `date` = '{$date}'";
    $result = mysqli_query($conn, $sql);
    $row = mysqli_fetch_all($result, 1);
    foreach ($row[0] as $item=>$value) {
        return $value;
    }
    // return $row;
}
function show_day_name_by_id($id){
    $conn = connectDB();
    $sql = 'SELECT `day` FROM `time`  WHERE `id` IN (' . implode(",", $id) . ')';

    $result = mysqli_query($conn,$sql);
    $row = mysqli_fetch_all($result,1);
    return $row;
}
function show_date_name_by_id($id){
    $conn = connectDB();
    $sql = "SELECT * FROM `time` WHERE `id`=$id";
    $result = mysqli_query($conn,$sql);
    return $result;
}


function add_patient($data,$time,$doctor_id,$hospital_id, $date){
    $conn = connectDB();
    $date = ADDate($date, '/');
    $date = str_replace('/', '-', $date);
    if (get_count($date) < 20) {
        $turn = get_count($date) + 1;
        $sql = "INSERT INTO `patient` (`id`, `name`, `lastname`, `national_code`, `tel`, `father_name`,
        `state`, `city`, `address`, `time_id`, `doctor_id`, `hospital_id`, `date`, `turns`) VALUES
        (NULL,
        '{$data['name']}',
        '{$data['lastname']}',
        '{$data['code']}',
        '{$data['tel']}',
        '{$data['fathername']}',
        '{$data['state_name']}',
        '{$data['city_name']}',
        '{$data['address']}',
        '{$time}',
        '{$doctor_id}',
        '{$hospital_id}',
        '{$date}',
        '{$turn}');";
        $result = mysqli_query($conn,$sql);
        if($result){
            return $turn;
        }
    }
}
function show_day_by_id($id){
    $conn = connectDB();
    $sql = "SELECT (`day`) FROM `time` WHERE `id`=$id";
    $result = mysqli_query($conn,$sql);
    $row = mysqli_fetch_assoc($result);
    foreach ($row as $id){
        switch($id){
            case '0':
                $day = "شنبه";
            break;
            case '1':
                $day = "یکشنبه";
            break;
            case '2':
                $day = "دوشنبه";
            break;
            case '3':
                $day = "سه شنبه";
            break;
            case '4':
                $day = "چهارشنبه";
            break;
            case '5':
                $day = "پنجشنبه";
            break;
    
        }
    }
   
    return $day;  
}
function show_hour_by_id($id){
    $conn = connectDB();
    $sql = "SELECT (`hour`) FROM `time` WHERE `id`=$id";
    $result = mysqli_query($conn,$sql);
    $row = mysqli_fetch_assoc($result);
    foreach ($row as $id){
        switch($id){
                
            case "1":
                $time = "صبح (ساعت 8 تا 10) ";
            break;
            case "1,2":
                $time = "صبح (ساعت 8 تا 10) <br><br> "."ظهر ( ساعت 12 تا 14) <br>";
            break;
            case "1,2,3":
                $time = "صبح (ساعت 8 تا 10)   <br><br>"."ظهر ( ساعت 12 تا 14)  <br><br>"."عصر ( ساعت 16 تا 18) ";                           ;
            break;
            case "2":
                $time = "ظهر ( ساعت 12 تا 14)";
            break;
            case "2,3":
                $time = "ظهر ( ساعت 12 تا 14) <br><br> "."عصر ( ساعت 16 تا 18)";
            break;
            case "3":
                $time = "عصر ( ساعت 16 تا 18)";
            break;
            case "1,3":
                $time = "صبح (ساعت 8 تا 10) <br><br> "."عصر ( ساعت 16 تا 18)";
            break;
        }
    }
   
    return $time;  
    
}
function show_patient_by_code($code){
    $conn = connectDB();
    $sql = "SELECT * FROM `patient` WHERE `national_code`=$code";
    $result = mysqli_query($conn,$sql);
    
    return $result;
    
}
function cancel_patient($id){
    $conn = connectDB();
    $sql = "DELETE FROM `patient` WHERE `id`=$id";
    $result = mysqli_query($conn,$sql);
    $_SESSION['cancel'] = "ok";
    header("Location: cancel.php");
    exit();
}
function show_doctor_name($id){
    $conn = connectDB();
    $sql = "SELECT * FROM `doctor` WHERE `id`=$id";
    $result = mysqli_query($conn,$sql);
    return $result;
}

function delete_patient(){
    
}

function persianDate($str, $exploder = '-', $isReverse = false)
{
    $date = explode($exploder, $str);
    if ($isReverse) {
        return gregorian_to_jalali($date[2], $date[1], $date[0], '/');
    }
    return gregorian_to_jalali($date[0], $date[1], $date[2], '/');
}

function ADDate($str, $exploder = '-', $isReverse = false)
{
    $date = explode($exploder, $str);
    if ($isReverse) {
        return jalali_to_gregorian($date[2], $date[1], $date[0], '/');
    }
    return jalali_to_gregorian($date[0], $date[1], $date[2], '/');
}

function fa2en($string)
{
    return strtr($string, array('۰' => '0', '۱' => '1', '۲' => '2', '۳' => '3', '۴' => '4', '۵' => '5', '۶' => '6', '۷' => '7', '۸' => '8', '۹' => '9', '٠' => '0', '١' => '1', '٢' => '2', '٣' => '3', '٤' => '4', '٥' => '5', '٦' => '6', '٧' => '7', '٨' => '8', '٩' => '9'));
}