<?php
require_once '../include/config.php';

function add_parent_hospital($name){
    $conn = connectDB();
    $sql = "INSERT INTO `hospital` (`name`) VALUES ('".$name."') ";
    $res = mysqli_query($conn,$sql);
    if ($res){
        $_SESSION['hospital'] = "add_parent";
    }
    header("Location: dashboard.php?m=hospital&p=listParentHospital");
    exit();
}
function show_parent_hospital($parent_id){
    $conn = connectDB();
	$menu = array();
	$sqlquery = " SELECT * FROM `hospital` where status='1' and parent_id='" .$parent_id . "' ";
	$res=mysqli_query($conn,$sqlquery);
    return $res;
}
function show_parent_list_hospital(){
    $conn = connectDB();
    $sql = "SELECT * FROM `hospital` WHERE parent_id=0";
    $result = mysqli_query($conn,$sql);
    return $result;
}
function delete_parent_hospital($id){
    $conn = connectDB();
    $sql = "DELETE FROM `hospital` WHERE id='".$id."'";
    mysqli_query($conn,$sql);
    $_SESSION['hospital'] = "parent_delete_ok";
    header("Location: dashboard.php?m=hospital&p=listParentHospital");
    exit();
}
function update_parent_hospital($id,$name){
    $conn = connectDB();
    $sql = "UPDATE `hospital` SET `name`='".$name."' WHERE `id`='".$id."'";
    mysqli_query($conn,$sql);
    $_SESSION['hospital'] = "parent_update_ok";
    header("Location: dashboard.php?m=hospital&p=listParentHospital");
    exit();
}
function show_parent_data($id){
    $conn = connectDB();
    $sql = "SELECT * FROM `hospital` WHERE id='".$id."'";
    $result = mysqli_query($conn,$sql);
    return $result;
}
function show_child_hospital(){
    
}

function add_hospital($name,$parent_id){
    $conn = connectDB();
    $sql = "INSERT INTO `hospital` (`name`,`parent_id`) VALUES ('".$name."','".$parent_id."') ";
    $res = mysqli_query($conn,$sql);
    if ($res){
        $_SESSION['hospital'] = "add_hospital";
    }
    header("Location: dashboard.php?m=hospital&p=listHospital");
    exit();
}
function show_hospital_list(){
    $conn = connectDB();
    $sql = "SELECT * FROM `hospital` WHERE parent_id != 0";
    $result = mysqli_query($conn,$sql);
    return $result;
}
function show_parent_name(){
    $conn = connectDB();
    $args = func_get_args();
    foreach ($args as $arg){
        
        $sql = "SELECT (`name`) FROM `hospital` WHERE id = $arg";
        $result = mysqli_query($conn,$sql);
        $row = mysqli_fetch_assoc($result);
    }
    return $row;
}
function show_hospital_data($id){
    $conn = connectDB();
    $sql = "SELECT * FROM `hospital` WHERE id='".$id."'";
    $result = mysqli_query($conn,$sql);
    return $result;
}
function get_parent_id_by_child_id($childID){
    $conn = connectDB();
    $sql = "SELECT (`parent_id`) FROM `hospital` WHERE id='".$childID."'";
    $result = mysqli_query($conn,$sql);
    $row = mysqli_fetch_assoc($result);
    return $row;
}
function update_hospital($id,$name,$parent){
    $conn = connectDB();
    $sql = "UPDATE `hospital` SET `name`='".$name."', `parent_id`='".$parent."' WHERE `id`='".$id."'";
    // echo "<pre dir='ltr'>";
    // echo $sql;
    // echo "</pre>";
    mysqli_query($conn,$sql);
    $_SESSION['hospital'] = "hospital_update_ok";
    header("Location: dashboard.php?m=hospital&p=listHospital");
    exit();
}
function delete_hospital($id){
    $conn = connectDB();
    $sql = "DELETE FROM `hospital` WHERE id='".$id."'";
    mysqli_query($conn,$sql);
    $_SESSION['hospital'] = "hospital_delete_ok";
    header("Location: dashboard.php?m=hospital&p=listHospital");
    exit();
}
