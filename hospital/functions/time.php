<?php
require_once '../include/config.php';

function addTime($day,$time){
    $conn = connectDB();
    // $times = "";
    // foreach ($time as $val){
    //     $times .= $val;
    // }
    $times = implode(",",$time);
    $sql = "INSERT INTO `time` (`day`,`hour`) VALUES ('".$day."','".$times."')";
    mysqli_query($conn,$sql);
    $_SESSION['time'] = "add_ok";
    header("Location: dashboard.php?m=time&p=listTime");
    exit();
}
function showTimeList(){
    $conn = connectDB();
    $sql = "SELECT * FROM `time`";
    $result = mysqli_query($conn,$sql);
    return $result;
}
function showTime($id){
    $conn = connectDB();
    $sql = "SELECT * FROM `time` WHERE id='".$id."'";
    $result = mysqli_query($conn,$sql);
    return $result;
}
function updateTime($day,$time,$id){
    $conn = connectDB();
    $times = implode(",",$time);
    $sql = "UPDATE `time` SET `day`='".$day."',`hour`='".$times."' WHERE `id`='".$id."'";
    mysqli_query($conn,$sql);
    $_SESSION['time'] = "update_ok";
    header("Location: dashboard.php?m=time&p=listTime");
    exit();
}
function deleteTime($id){
    $conn = connectDB();
    $sql = "DELETE FROM `time` WHERE id='".$id."'";
    mysqli_query($conn,$sql);
    $_SESSION['time'] = "delete_ok";
    header("Location: dashboard.php?m=time&p=listTime");
    exit();
}
