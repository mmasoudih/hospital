<?php

function add_doctor($name,$lastname,$expertise,$hospital,$date,$image){
    $conn = connectDB();
    $time = implode(",",$date);

    $sql = "INSERT INTO `doctor` (`name`,`lastname`,`expertise`,`image`,`date`,`hospital_id`) VALUES ('".$name."','".$lastname."','".$expertise."','".$image."','".$time."','".$hospital."')";

    $result = mysqli_query($conn,$sql);
    if($result){
        $_SESSION['doctor'] = "add_doctor_ok";
        header("Location: dashboard.php?m=doctor&p=listDoctor");
        exit();
    }
    
}
function upload_picture_doctor($files){
    
    $target_dir = "./uploads/";
    $old_name = explode(".",$files["name"]);
    $new_name = "img-".time().rand(rand(),rand());
    $ext = end($old_name);
    $final = $new_name.'.'.$ext;

    $target_file = $target_dir . basename($final);
    
    $uploadOk = 1;
    $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

    // Check if file already exists
    if (file_exists($target_file)) {
        echo "فایل از قبل وجود دارد.";
        $uploadOk = 0;
    }
    // Check file size
    if ($files["size"] > 500000) {
        echo "حجم فایل خیلی زیاد است.";
        $uploadOk = 0;
    }
    // Allow certain file formats
    if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
    && $imageFileType != "gif" ) {
        echo "متاسفانه فقط پسوند های : JPG, JPEG, PNG & GIF مجاز هستند";
        
        $uploadOk = 0;
    }
    // Check if $uploadOk is set to 0 by an error
    if ($uploadOk == 0) {
        echo "متاسفانه فایل اپلود نشد";
    // if everything is ok, try to upload file
    } else {
        if (move_uploaded_file($files["tmp_name"], $target_file)) {

        } else {
            echo "Sorry, there was an error uploading your file.";
        }
    }
    return $target_file;
}
function show_doctor(){
    $conn = connectDB();
    $sql = "SELECT * FROM `doctor`";
    $result = mysqli_query($conn,$sql);
    return $result;
}
function show_hospital_by_id($id){
    $conn = connectDB();
    $sql = "SELECT (`name`) FROM `hospital` WHERE id='".$id."'";
    $result = mysqli_query($conn,$sql);
    $row = mysqli_fetch_assoc($result);
    return $row;
}
function show_doctor_update($id){
    $conn = connectDB();
    $sql = "SELECT * FROM `doctor` WHERE id='".$id."'";
    $result = mysqli_query($conn,$sql);
    return $result;
}
function show_doctor_data_update($id){
    $conn = connectDB();
    $sql = "SELECT * FROM `doctor` WHERE id='".$id."'";
    $result = mysqli_query($conn,$sql);
    return $result;
}
function update_doctor($id,$name,$lastname,$expertise,$hospital,$time,$pic){
    $conn = connectDB();
    $times = implode(",",$time);
    $sql = "UPDATE `doctor` SET `name`='".$name."',`lastname`='".$lastname."',`expertise`='".$expertise."',`image`='".$pic."',`date`='".$times."',`hospital_id`='".$hospital."' WHERE `id`='".$id."'";
    // echo "<pre dir='ltr'>";
    // print_r($sql);
    // echo "</pre>";
    mysqli_query($conn,$sql);
    $_SESSION['doctor'] = "update_doctor_ok";
    header("Location: dashboard.php?m=doctor&p=listDoctor");
    exit();
}
function get_image_for_delete($id){
    $conn = connectDB();
    $sql = "SELECT (`image`) FROM `doctor` WHERE id='".$id."'";
    $result = mysqli_query($conn,$sql);
    $row = mysqli_fetch_assoc($result);
    return $row;
}
function delete_doctor($id){
    $conn = connectDB();
    $user_image = implode(" ",get_image_for_delete($id));
    unlink($user_image);
    $sql = "DELETE FROM `doctor` WHERE id='".$id."'";
    mysqli_query($conn,$sql);
    $_SESSION['doctor'] = "delete_doctor_ok";
    header("Location: dashboard.php?m=doctor&p=listDoctor");
    exit();
}
function add_date(){
    
}
