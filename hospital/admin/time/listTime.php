<?php
if(isset($_SESSION['time']) && $_SESSION['time'] == "update_ok"){
    echo '
    <div class="alert alert-block alert-success fade in large-fontsize">
        <strong>تبریک :</strong> بروزرسانی با موفقیت انجام شد.
    </div>
    ';
    unset($_SESSION['time']);
}


if(isset($_SESSION['time']) && $_SESSION['time'] == "add_ok"){
    echo '
    <div class="alert alert-block alert-success fade in large-fontsize">
        <strong>تبریک :</strong> زمان با موفقیت اضافه شد.
    </div>
    ';
    unset($_SESSION['time']);
}

if(isset($_SESSION['time']) && $_SESSION['time'] == "delete_ok"){
    echo '
    <div class="alert alert-block alert-success fade in large-fontsize">
        <strong>تبریک :</strong> زمان با موفقیت حذف شد.
    </div>
    ';
    unset($_SESSION['time']);
}
?>
<div class="row">
    <div class="col-lg-12">
        <section class="panel">
        <?php 
        $row = showTimeList();
        if(mysqli_num_rows($row) > 0):
        ?>
            <header class="panel-heading">
                جدول زمانی پزشک ها
            
            </header>
            

            <table class="table table-striped table-advance table-hover">
                <thead>
                    <tr>
                        <th><i class="icon-bullhorn"></i>روز هفته</th>
                        <th class="hidden-phone"><i class="icon-question-sign"></i>ساعت پذیرش</th>
                        <th>ویرایش/حذف</th>
                    </tr>
                </thead>
                <tbody>
                <?php
                    
                    
                    $day = NULL;
                    $time = NULL;
                    
                    while($data = mysqli_fetch_assoc($row)):
                    switch($data['day']){
                        case '0':
                            $day = "شنبه";
                        break;
                        case '1':
                            $day = "یکشنبه";
                        break;
                        case '2':
                            $day = "دوشنبه";
                        break;
                        case '3':
                            $day = "سه شنبه";
                        break;
                        case '4':
                            $day = "چهارشنبه";
                        break;
                        case '5':
                            $day = "پنجشنبه";
                        break;

                    }
                    switch($data['hour']){
                        
                        case "1":
                            $time = "صبح (ساعت 8 تا 10)";
                        break;
                        case "1,2":
                            $time = "صبح (ساعت 8 تا 10) - "."ظهر ( ساعت 12 تا 14)";
                        break;
                        case "1,2,3":
                            $time = "صبح (ساعت 8 تا 10) -  "."ظهر ( ساعت 12 تا 14) - "."عصر ( ساعت 16 تا 18)";                           ;
                        break;
                        case "2":
                            $time = "ظهر ( ساعت 12 تا 14)";
                        break;
                        case "2,3":
                            $time = "ظهر ( ساعت 12 تا 14) - "."عصر ( ساعت 16 تا 18)";
                        break;
                        case "3":
                            $time = "عصر ( ساعت 16 تا 18)";
                        break;
                        case "1,3":
                            $time = "صبح (ساعت 8 تا 10) - "."عصر ( ساعت 16 تا 18)";
                        break;
                    }
                ?>
                    <tr>
                        <td><?php echo $day; ?></td>
                        <td><?php echo $time; ?></td>
                        
                        
                        <td>
                            
                            <a href="dashboard.php?m=time&p=updateTime&id=<?php echo $data['id']; ?>"><button class="btn btn-primary btn-xs"><i class="icon-pencil"></i></button></a>
                            <a href="dashboard.php?m=time&p=deleteTime&id=<?php echo $data['id']; ?>"><button class="btn btn-danger btn-xs"><i class="icon-trash "></i></button></a>
                        </td>
                    </tr>
                    

                    
                <?php
                    endwhile;
                ?>
                
                    
                </tbody>
            </table>

            <?php
            else: 
                echo '    
                <div class="alert alert-block alert-info fade in large-fontsize">
                <strong>راهنمایی :</strong> از قسمت جدول زمان بندی ابتدا یک زمان اضافه کنید.
                </div>';
            endif;
            ?>
        </section>
    </div>
</div>
