<?php
if (isset($_POST['btn'])){
    $day = $_POST['day'];
    @$time = $_POST['time'];
    if($time == ""){
        echo '<div class="alert alert-block alert-warning fade in">
           
        <strong>هشدار :</strong> لطفا یک ساعت را انتخاب کنید.
  
    </div>';
    }else{
        addTime($day,$time);
    }
}
?>
<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">
                اضافه کردن زمان جدید
            
            </header>
            <div class="panel-body">
                <form role="form" method="post">
                    

                    <div class="form-group">
                        <label class="control-label" for="inputSuccess">انتخاب روز :</label>
                        
                        <select class="form-control m-bot15 p" name="day">
                                <option value="0">شنبه</option>
                                <option value="1">یکشنبه</option>
                                <option value="2">دوشنبه</option>
                                <option value="3">سه‌شنبه</option>
                                <option value="4">چهارشنبه</option>
                                <option value="5">پنجشنبه</option>
                            </select>
                    </div>

                    <div class="form-group">
                        <label class="control-label" for="inputSuccess">انتخاب ساعت :</label>
                            <select multiple="" class="form-control" name="time[]">
                                <option value="1">صبح (ساعت 8 تا 10)</option>
                                <option value="2">ظهر ( ساعت 12 تا 14)</option>
                                <option value="3">عصر ( ساعت 16 تا 18)</option>
                            </select>     
                    </div>
                    <button type="submit" class="btn btn-info" name="btn"> اضافه کردن زمان</button>
                </form>

            </div>
        </section>
    </div>
</div>
