
<?php

    
    $row =showTime($_GET['id']);
    $data = mysqli_fetch_assoc($row);
    $arrayHour = explode(",",$data['hour']);
if (isset($_POST['btn'])){
    $day = $_POST['day'];
    $time = $_POST['time'];
    $id = $_GET['id'];
    updateTime($day,$time,$id);
    
}
?>
<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">
                اضافه کردن زمان جدید
            
            </header>
            <div class="panel-body">
                <form role="form" method="post">
                    

                    <div class="form-group">
                        <label class="control-label" for="inputSuccess">انتخاب روز :</label>
                        
                        <select class="form-control m-bot15 p" name="day">
                                <option value="0" <?php if($data['day'] == 0){echo 'selected = "selected"';} ?>>شنبه</option>
                                <option value="1" <?php if($data['day'] == 1){echo 'selected = "selected"';} ?>>یکشنبه</option>
                                <option value="2" <?php if($data['day'] == 2){echo 'selected = "selected"';} ?>>دوشنبه</option>
                                <option value="3" <?php if($data['day'] == 3){echo 'selected = "selected"';} ?>>سه‌شنبه</option>
                                <option value="4" <?php if($data['day'] == 4){echo 'selected = "selected"';} ?>>چهارشنبه</option>
                                <option value="5" <?php if($data['day'] == 5){echo 'selected = "selected"';} ?>>پنجشنبه</option>
                            </select>
                    </div>

                    <div class="form-group">
                        <label class="control-label" for="inputSuccess">انتخاب ساعت :</label>
                            <select multiple="" class="form-control" name="time[]">
                                <option value="1" <?php if(in_array(1,$arrayHour)){echo 'selected = "selected"';} ?>>صبح (ساعت 8 تا 10)</option>
                                <option value="2" <?php if(in_array(2,$arrayHour)){echo 'selected = "selected"';} ?>>ظهر ( ساعت 12 تا 14)</option>
                                <option value="3" <?php if(in_array(3,$arrayHour)){echo 'selected = "selected"';} ?>>عصر ( ساعت 16 تا 18)</option>
                            </select>     
                    </div>
                    <button type="submit" class="btn btn-info" name="btn"> به روزرسانی زمان</button>
                </form>

            </div>
        </section>
    </div>
</div>
