<?php
$res = show_profile_data();
$row = mysqli_fetch_assoc($res);
    if(isset($_POST['btn'])){
        $data = $_POST['frm'];
        update_profile_data($data);
         
    }
?>

<div class="row">
<div class="col-lg-12">
<?php     
    if(isset($_SESSION['update']) && $_SESSION['update'] = "update_profile_ok"){
        echo '
        <div class="alert alert-block alert-success fade in large-fontsize">
            <strong>تبریک :</strong> بروزرسانی با موفقیت انجام شد.
        </div>
        ';
        unset($_SESSION['update']);
    }
    if(isset($_SESSION['password']) && $_SESSION['password'] = "update_profile_ok"){
        echo '
        <div class="alert alert-block alert-danger fade in large-fontsize">
            <strong>اخطار :</strong> رمز عبور و تکرار آن یکسان نیستند.
        </div>
        ';
        unset($_SESSION['password']);
    }
?>
    <section class="panel">
    
        <header class="panel-heading">
            تغییر تنظیمات پروفایل
        
        </header>
        <div class="panel-body">
            <form role="form" method="post">
                <div class="form-group">
                    <label for="name"> نام :</label>
                    <input type="text" class="form-control" id="name" name="frm[name]" placeholder="نام خود را وارد کنید." value="<?php echo $row['name']; ?>">
                </div>
                <div class="form-group">
                    <label for="lastname">نام خانوادگی :</label>
                    <input type="text" class="form-control" id="lastname" name="frm[lastname]" placeholder="نام خانوادگی خود را وارد کنید." value="<?php echo $row['lastname']; ?>">
                </div>
                <div class="form-group">
                    <label for="username">نام کاربری :</label>
                    <input type="text" class="form-control" id="username" name="frm[username]" placeholder="نام کاربری خود را وارد کنید." value="<?php echo $row['username']; ?>">
                </div>
                <div class="form-group">
                    <label for="password">رمز عبور :</label>
                    <input type="password" class="form-control" id="password" name="frm[password]" placeholder="رمز عبور خود را وارد کنید." value="<?php echo $row['password']; ?>">
                </div>
                <div class="form-group">
                    <label for="password"> تکرار رمز عبور :</label>
                    <input type="password" class="form-control" id="password" name="frm[password_confrim]" placeholder=" تکرار رمز عبور خود را وارد کنید." value="<?php echo $row['password']; ?>">
                </div>
                
                
                
                <button type="submit" class="btn btn-info" name="btn">بروزرسانی پروفایل من </button>
            </form>

        </div>
    </section>
</div>
</div>