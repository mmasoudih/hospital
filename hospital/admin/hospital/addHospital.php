<?php 
    if(isset($_POST['btn'])){
        $name = $_POST['hospital_name'];
        $parent_id = $_POST['parent'];
        add_hospital($name,$parent_id);
    }
    $row = show_parent_hospital(0);
?>
<div class="row">
    <div class="col-lg-12">
        <section class="panel">
        <?php 
                if(!mysqli_num_rows($row) <= 0):
        ?>
            <header class="panel-heading">
                اضافه کردن بیمارستان جدید
            
            </header>
            <div class="panel-body">
            
                <form role="form" method="post">
                    <div class="form-group">
                        <label for="name">نام بیمارستان:</label>
                        <input type="text" class="form-control" id="name" name="hospital_name" placeholder="نام بیمارستان را وارد کنید">
                    </div>

                    <div class="form-group">
                        <label class="control-label" for="inputSuccess">انتخاب نوع بیمارستان :</label>
                        
                        <select class="form-control m-bot15 p" name="parent">
                            <?php 
                                
                                while($data = mysqli_fetch_assoc($row)):
                            ?>
                            <option value="<?php echo $data['id'] ?>"><?php echo $data['name']; ?></option>
                            <?php
                                endwhile;
                            ?>
                        </select>
                    </div>
                    <button type="submit" class="btn btn-info" name="btn">اضافه کردن بیمارستان</button>
                </form>
            
            </div>
            <?php
            else: 
                echo '    
                <div class="alert alert-block alert-info fade in large-fontsize">
                <strong>راهنمایی :</strong> از قسمت مدیریت بیمارستان ها ابتدا یک دسته بندی اضافه کنید.
                </div>'; 
                endif;
            ?>
        </section>
    </div>
</div>