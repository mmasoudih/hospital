<?php 
if(isset($_GET['id']) && $_GET['id'] !== ""){
    $id = $_GET['id'];
    $row = show_parent_data($id);
    $data = mysqli_fetch_assoc($row);
}else{
    header("Location: dashboard.php?m=hospital&p=listParentHospital");
    exit();
}
if (isset($_POST['btn'])){
    $name = $_POST['name'];
    update_parent_hospital($id,$name);
}
?>
<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">
              ایجاد دسته برای بیمارستان
            
            </header>
            <div class="panel-body">
                <form role="form" method="post">
                    <div class="form-group">
                        <label for="name">نوع بیمارستان را وارد کنید:</label>
                        <input type="text" class="form-control" id="name" name="name" placeholder=" نوع بیمارستان را وارد کنید" value="<?php echo $data['name']; ?>">
                    </div>

                    <button type="submit" class="btn btn-info" name="btn">بروزرسانی دسته بیمارستان</button>
                </form>

            </div>
        </section>
    </div>
</div>