<?php
if(isset($_SESSION['time']) && $_SESSION['time'] == "update_ok"){
    echo '
    <div class="alert alert-block alert-success fade in large-fontsize">
        <strong>تبریک :</strong> بروزرسانی با موفقیت انجام شد.
    </div>
    ';
    unset($_SESSION['time']);
}

if(isset($_SESSION['time']) && $_SESSION['time'] == "add_ok"){
    echo '
    <div class="alert alert-block alert-success fade in large-fontsize">
        <strong>تبریک :</strong> زمان با موفقیت اضافه شد.
    </div>
    ';
    unset($_SESSION['time']);
}

if(isset($_SESSION['time']) && $_SESSION['time'] == "delete_ok"){
    echo '
    <div class="alert alert-block alert-success fade in large-fontsize">
        <strong>تبریک :</strong> زمان با موفقیت حذف شد.
    </div>
    ';
    unset($_SESSION['time']);
}
?>
<div class="row">
    <div class="col-lg-12">
        <section class="panel">
        <?php 
        $row = show_parent_list_hospital();
        if(mysqli_num_rows($row) > 0):
        ?>
            <header class="panel-heading">
               لیست دسته بندی های بیمارستان ها
            </header>
            <table class="table table-striped table-advance table-hover">
                <thead>
                    <tr>
                        <th><i class="icon-bullhorn"></i>نام دسته بندی</th>
                        <th>ویرایش/حذف</th>
                    </tr>
                </thead>
                <tbody>
                <?php
                    while($data = mysqli_fetch_assoc($row)):                    
                ?>
                    <tr>
                        <td><?php echo $data['name']; ?></td>
                        <td>
                            <a href="dashboard.php?m=hospital&p=updateParentHospital&id=<?php echo $data['id']; ?>"><button class="btn btn-primary btn-xs"><i class="icon-pencil"></i></button></a>
                            <a href="dashboard.php?m=hospital&p=deleteParentHospital&id=<?php echo $data['id']; ?>"><button class="btn btn-danger btn-xs"><i class="icon-trash "></i></button></a>
                        </td>
                    </tr>
                <?php
                    endwhile;
                ?> 
                </tbody>
            </table>
            <?php
            else: 
                echo '    
                <div class="alert alert-block alert-info fade in large-fontsize">
                <strong>راهنمایی :</strong> از قسمت مدیریت بیمارستان ها ابتدا یک دسته بندی اضافه کنید.
                </div>';
            endif;
            ?>
        </section>
    </div>
</div>
