<?php 
if(isset($_GET['id']) && $_GET['id'] !== ""){
    $id = $_GET['id'];
    $row = show_hospital_data($id);
    $data = mysqli_fetch_assoc($row);
    $parent_id = get_parent_id_by_child_id($_GET['id']);
    foreach($parent_id as $a){
        $parent_id = $a;
    }
}else{
    header("Location: dashboard.php?m=hospital&p=listHospital");
    exit();
}
if (isset($_POST['btn'])){
    $name = $_POST['hospital_name'];
    $parent = $_POST['parent'];
    update_hospital($id,$name,$parent);
}
?>
<div class="row">
    <div class="col-lg-12">
        <section class="panel">
       
            <header class="panel-heading">
              ویرایش اطلاعات بیمارستان
            
            </header>
            <div class="panel-body">
            
                <form role="form" method="post">
                    <div class="form-group">
                        <label for="name">نام بیمارستان:</label>
                        <input type="text" class="form-control" id="name" name="hospital_name" placeholder="نام بیمارستان را وارد کنید" value="<?php echo $data['name']; ?>"> 
                    </div>

                    <div class="form-group">
                        <label class="control-label" for="inputSuccess">انتخاب نوع بیمارستان :</label>
                        <select class="form-control m-bot15 p" name="parent">
                            <?php
                             $row = show_parent_hospital(0); 
                             while($data =mysqli_fetch_array($row)):
                            ?>
                            <option value="<?php echo $data['id'] ?>" <?php if($parent_id == $data['id']){echo 'selected = "selected"';} ?>><?php echo $data['name']; ?></option>
                            <?php
                             endwhile;
                            ?>                    
                        </select>
                    </div>
                    <button type="submit" class="btn btn-info" name="btn"> ویرایش بیمارستان</button>
                </form>
            
            </div>
           
            
        </section>
    </div>
</div>