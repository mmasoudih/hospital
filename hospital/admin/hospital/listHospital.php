<?php
if(isset($_SESSION['time']) && $_SESSION['time'] == "update_ok"){
    echo '
    <div class="alert alert-block alert-success fade in large-fontsize">
        <strong>تبریک :</strong> بروزرسانی با موفقیت انجام شد.
    </div>
    ';
    unset($_SESSION['time']);
}

if(isset($_SESSION['time']) && $_SESSION['time'] == "add_ok"){
    echo '
    <div class="alert alert-block alert-success fade in large-fontsize">
        <strong>تبریک :</strong> زمان با موفقیت اضافه شد.
    </div>
    ';
    unset($_SESSION['time']);
}

if(isset($_SESSION['time']) && $_SESSION['time'] == "delete_ok"){
    echo '
    <div class="alert alert-block alert-success fade in large-fontsize">
        <strong>تبریک :</strong> زمان با موفقیت حذف شد.
    </div>
    ';
    unset($_SESSION['time']);
}
?>
<div class="row">
    <div class="col-lg-12">
        <section class="panel">
        <?php 
        $row = show_hospital_list();
        if(mysqli_num_rows($row) > 0):
        ?>
            <header class="panel-heading">
               لیست بیمارستان ها
            </header>
            <table class="table table-striped table-advance table-hover">
                <thead>
                    <tr>
                        <th><i class="icon-bullhorn"></i>نام بیمارستان</th>
                        <th><i class="icon-bullhorn"></i>نام دسته</th>
                        <th>ویرایش/حذف</th>
                    </tr>
                </thead>
                <tbody>
                <?php
                
                    while($data = mysqli_fetch_assoc($row)):
                        $a = $data['parent_id'];
                ?>
                    <tr>
                        <td><?php echo $data['name']; ?></td>
                        <td><?php if(@!implode("",show_parent_name($a))){echo "هیچ دسته بندی وجود ندارد";} else{echo implode("",show_parent_name($a));} ?></td>
                        <td>
                            <a href="dashboard.php?m=hospital&p=updateHospital&id=<?php echo $data['id']; ?>"><button class="btn btn-primary btn-xs"><i class="icon-pencil"></i></button></a>
                            <a href="dashboard.php?m=hospital&p=deleteHospital&id=<?php echo $data['id']; ?>"><button class="btn btn-danger btn-xs"><i class="icon-trash "></i></button></a>
                        </td>
                    </tr>
                <?php
                    endwhile;
                ?> 
                </tbody>
            </table>
            <?php
            else: 
                echo '    
                <div class="alert alert-block alert-info fade in large-fontsize">
                <strong>راهنمایی :</strong> از قسمت مدیریت بیمارستان ها ابتدا یک دسته بندی اضافه کنید.
                </div>';
            endif;
            ?>
        </section>
    </div>
</div>
