<?php 
if(isset($_POST['btn'])){
    
    // echo "<pre dir='ltr'>";
    // print_r($_POST);
    // echo "</pre>";
    $name = $_POST['name'];
    $lastname = $_POST['lastname'];
    $hospital = $_POST['hospital'];
    $time = $_POST['time'];
    $expertise = $_POST['expertise'];
    if(empty($_FILES['picture']['name'])){
        
        add_doctor($name,$lastname,$expertise,$hospital,$time,"../assets/img/doctor-default.png");
    }else{
        add_doctor($name,$lastname,$expertise,$hospital,$time,upload_picture_doctor($_FILES['picture']));
    }
    
    

}


?>
<div class="row">
    <div class="col-lg-12">
        <section class="panel">
        <?php 
        $hospital_check = show_hospital_list();
        $time_check = showTimeList();
        if(mysqli_num_rows($time_check) > 0 || mysqli_num_rows($hospital_check) > 0):
        ?>
            <header class="panel-heading">
                اضافه کردن دکتر جدید
            
            </header>
            <div class="panel-body">
                <form role="form" method="post"  enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="name">نام پزشک:</label>
                        <input type="text" class="form-control" id="name" name="name" placeholder="نام پزشک را وارد کنید">
                    </div>
                    <div class="form-group">
                        <label for="name">نام خانوادگی پزشک:</label>
                        <input type="text" class="form-control" id="name" name="lastname" placeholder="نام خانوادگی پزشک را وارد کنید">
                    </div>
                    <div class="form-group">
                        <label for="name">تخصص پزشک:</label>
                        <input type="text" class="form-control" id="name" name="expertise" placeholder=" تخصص پزشک را وارد کنید">
                    </div>

                    

                    <div class="form-group">
                        <label class="control-label" for="inputSuccess">انتخاب بیمارستان :</label>
                        
                        <select class="form-control m-bot15 p" name="hospital">
                        <?php 
                            $row = show_hospital_list();
                            while($data = mysqli_fetch_assoc($row)):
                        ?>
                                <option value="<?php echo $data['id']; ?>"><?php echo $data['name']; ?></option>
                                
                        <?php 
                            endwhile;
                        ?>
                            </select>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="inputSuccess">انتخاب روز های پذیرش :</label>
                            <select multiple="" class="form-control" name="time[]" height="300px">
                                <?php
                                    $row = showTimeList();
                                    
                        
                        
                                $day = NULL;
                                $time = NULL;
                                
                                while($data = mysqli_fetch_assoc($row)):
                                switch($data['day']){
                                    case '0':
                                        $day = "شنبه";
                                    break;
                                    case '1':
                                        $day = "یکشنبه";
                                    break;
                                    case '2':
                                        $day = "دوشنبه";
                                    break;
                                    case '3':
                                        $day = "سه شنبه";
                                    break;
                                    case '4':
                                        $day = "چهارشنبه";
                                    break;
                                    case '5':
                                        $day = "پنجشنبه";
                                    break;

                                }
                                switch($data['hour']){
                                    
                                    case "1":
                                        $time = "صبح (ساعت 8 تا 10)";
                                    break;
                                    case "1,2":
                                        $time = "صبح (ساعت 8 تا 10) - "."ظهر ( ساعت 12 تا 14)";
                                    break;
                                    case "1,2,3":
                                        $time = "صبح (ساعت 8 تا 10) -  "."ظهر ( ساعت 12 تا 14) - "."عصر ( ساعت 16 تا 18)";                           ;
                                    break;
                                    case "2":
                                        $time = "ظهر ( ساعت 12 تا 14)";
                                    break;
                                    case "2,3":
                                        $time = "ظهر ( ساعت 12 تا 14) - "."عصر ( ساعت 16 تا 18)";
                                    break;
                                    case "3":
                                        $time = "عصر ( ساعت 16 تا 18)";
                                    break;
                                    case "1,3":
                                        $time = "صبح (ساعت 8 تا 10) - "."عصر ( ساعت 16 تا 18)";
                                    break;
                                }
                            ?>
                            <option value="<?php echo $data['id']; ?>"><?php echo $day." *** ".$time; ?></option>
                            <?php
                                endwhile;
                            ?>
                        </select>     
                    </div>   
                    <div class="form-group">
                        <label for="exampleInputFile">تصویر پزشک:</label>
                        <input type="file" id="exampleInputFile" name="picture">
                        <p class="help-block">یک تصویر برای پزشک انتخاب کنید.</p>
                    </div>
                    
                    <button type="submit" class="btn btn-info" name="btn">اضافه کردن</button>
                </form>

            </div>
            <?php 
            else: 
                echo '    
                <div class="alert alert-block alert-info fade in large-fontsize">
                <strong>راهنمایی :</strong> از قسمت جدول زمان بندی ابتدا زمان اضافه کنید و همچنین یک بیمارستان از بخش مدیریت بیمارستان اضافه کنید.
                </div>';
                endif;
            ?>
        </section>
    </div>
</div>