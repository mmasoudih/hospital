<?php 
if(isset($_GET['id']) && $_GET['id'] !==""){
    $data = show_doctor_update($_GET['id']);
    $row = show_doctor_data_update($_GET['id']);
    $results = mysqli_fetch_assoc($row);
}else{
    header("Location: dashboard.php?m=doctor&p=listDoctor");
    exit();
}
if(isset($_POST['btn'])){
    $name = $_POST['name'];
    $lastname = $_POST['lastname'];
    $hospital = $_POST['hospital'];
    $time = $_POST['time'];
    $expertise = $_POST['expertise'];
    if(empty($_FILES['picture']['name'])){
        $imageDB = $results['image'];
        update_doctor($_GET['id'],$name,$lastname,$expertise,$hospital,$time,$imageDB);
    }else{
        $imageUSER = upload_picture_doctor($_FILES['picture']);
        update_doctor($_GET['id'],$name,$lastname,$expertise,$hospital,$time,$imageUSER);
    }
}
$array = explode(",",$results['date']);
$txt = "selected='selected'";
?>
<div class="row">
    <div class="col-lg-12">
        <section class="panel">

            <header class="panel-heading">
                ویرایش اطلاعات دکتر
            
            </header>
            <div class="panel-body">
                <form role="form" method="post"  enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="name">نام پزشک:</label>
                        <input type="text" class="form-control" id="name" name="name" placeholder="نام پزشک را وارد کنید" value="<?php echo $results['name']; ?>">
                    </div>
                    <div class="form-group">
                        <label for="name">نام خانوادگی پزشک:</label>
                        <input type="text" class="form-control" id="name" name="lastname" placeholder="نام خانوادگی پزشک را وارد کنید" value="<?php echo $results['lastname']; ?>">
                    </div>
                    <div class="form-group">
                        <label for="name">تخصص پزشک:</label>
                        <input type="text" class="form-control" id="name" name="expertise" placeholder=" تخصص پزشک را وارد کنید" value="<?php echo $results['expertise']; ?>">
                    </div>

                    

                    <div class="form-group">
                        <label class="control-label" for="inputSuccess">انتخاب بیمارستان :</label>
                        
                        <select class="form-control m-bot15 p" name="hospital">
                        <?php 
                            $row = show_hospital_list();
                            while($data = mysqli_fetch_assoc($row)):
                        ?>
                                <option value="<?php echo $data['id']; ?>" <?php if($data['id'] == $results['hospital_id']){echo $txt;}?>><?php echo $data['name']; ?></option>
                                
                        <?php 
                            endwhile;
                        ?>
                            </select>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="inputSuccess">انتخاب روز های پذیرش :</label>
                            <select multiple="" class="form-control" name="time[]" height="300px">
                                <?php
                                    $row = showTimeList();
                                    
                        
                        
                                $day = NULL;
                                $time = NULL;
                                
                                while($data = mysqli_fetch_assoc($row)):
                                switch($data['day']){
                                    case '0':
                                        $day = "شنبه";
                                    break;
                                    case '1':
                                        $day = "یکشنبه";
                                    break;
                                    case '2':
                                        $day = "دوشنبه";
                                    break;
                                    case '3':
                                        $day = "سه شنبه";
                                    break;
                                    case '4':
                                        $day = "چهارشنبه";
                                    break;
                                    case '5':
                                        $day = "پنجشنبه";
                                    break;

                                }
                                switch($data['hour']){
                                    
                                    case "1":
                                        $time = "صبح (ساعت 8 تا 10)";
                                    break;
                                    case "1,2":
                                        $time = "صبح (ساعت 8 تا 10) - "."ظهر ( ساعت 12 تا 14)";
                                    break;
                                    case "1,2,3":
                                        $time = "صبح (ساعت 8 تا 10) -  "."ظهر ( ساعت 12 تا 14) - "."عصر ( ساعت 16 تا 18)";                           ;
                                    break;
                                    case "2":
                                        $time = "ظهر ( ساعت 12 تا 14)";
                                    break;
                                    case "2,3":
                                        $time = "ظهر ( ساعت 12 تا 14) - "."عصر ( ساعت 16 تا 18)";
                                    break;
                                    case "3":
                                        $time = "عصر ( ساعت 16 تا 18)";
                                    break;
                                    case "1,3":
                                        $time = "صبح (ساعت 8 تا 10) - "."عصر ( ساعت 16 تا 18)";
                                    break;
                                }
                            ?>
                            <option value="<?php echo $data['id']; ?>" <?php if(in_array($data['id'],$array)){echo $txt;}?>><?php echo $day." *** ".$time; ?></option>
                            <?php
                                endwhile;
                            ?>
                        </select>     
                    </div>   
                    <div class="form-group">
                        <label for="exampleInputFile">تصویر پزشک:</label>
                        <input type="file" id="exampleInputFile" name="picture">
                        
                    </div>
                    <div class="form-group">
                        <div class="user-heading alt"><a href="<?php echo $results['image']; ?>" target="_blank"><img src="<?php echo $results['image']; ?>" alt="" ></a></div>
                    </div>
                    
                    <button type="submit" class="btn btn-info" name="btn"> بروزرسانی اطلاعات پزشک</button>
                </form>

            </div>
        </section>
    </div>
</div>