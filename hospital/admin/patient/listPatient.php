
<div class="row">
    <div class="col-lg-12">
        <section class="panel">
        <?php 
        function show_all_patient(){
            $conn = connectDB();
            $sql = "SELECT * FROM `patient` ";
            $result = mysqli_query($conn,$sql);
            return $result; 
        }
        $row = show_all_patient();
        
        
        if(mysqli_num_rows($row) > 0):
        ?>
            <header class="panel-heading">
               لیست بیمارستان ها
            </header>
            <table class="table table-striped table-advance table-hover">
                <thead>
                    <tr>
                        <th><i class=""></i>#</th>
                        <th><i class=""></i>نام :</th>
                        <th><i class=""></i>نام خانوادگی :</th>
                        <th><i class=""></i>کد ملی :‌</th>
                        <th><i class=""></i>تلفن : ‌</th>
                        <th><i class=""></i>نام پدر‌ :</th>
                        <th><i class=""></i>استان : ‌</th>
                        <th><i class=""></i>شهر : ‌</th>
                        
                    </tr>
                </thead>
                <tbody>
                <?php
                
                    while($data = mysqli_fetch_assoc($row)):
                     
                ?>
                    <tr>
                        <td><?php echo $data['id']."#"  ; ?></td>
                        <td><?php echo $data['name']; ?></td>
                        <td><?php echo $data['lastname']; ?></td>
                        <td><?php echo $data['national_code']; ?></td>
                        <td><?php echo $data['tel']; ?></td>
                        <td><?php echo $data['father_name']; ?></td>
                        <td><?php echo $data['state']; ?></td>
                        <td><?php echo $data['city']; ?></td>

                        
                    </tr>
                <?php
                    endwhile;
                ?> 
                </tbody>
            </table>
            <?php
            else: 
                echo '    
                <div class="alert alert-block alert-warning fade in large-fontsize">
                <strong>هشدار :</strong> هیچ بیماری در سیستم موجود نیست
                </div>';
            endif;
            ?>
        </section>
    </div>
</div>
