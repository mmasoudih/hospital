<?php
include_once 'functions/patient.php';

if(isset($_POST['btn'])){
    $data = $_POST['frm'];
    $time = $_GET['date'];
    $doctor_id = $_GET['doctor_id'];
    $hospital_id = $_GET['type'];
    $turn = add_patient($data,$time,$doctor_id,$hospital_id, $_GET['time']);
}
?>
<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="keywords"
        content="بیمارستان شریعتی, بیمارستان شریعتی تهران, نوبت دهی شریعتی, نوبت دهی اینترنتی شریعتی,نوبت دهی اینترنتی درمانگاه های بیمارستان شریعتی">
    <title>نوبت دهی اینترنتی درمانگاه های بیمارستان شریعتی</title>

    <link href="assets/css/css.css" rel="stylesheet">
    <link href="assets/css/styles.css" rel="stylesheet">

    <link href="assets/fonts/css/all.css" rel="stylesheet">
    <script src="admin/js/city.js"></script>

</head>

<body>
    <header>
        <div class="container header0">
            <div class="row">
                <div class="col col-lg-11 text-right">

                </div>
                <div class="col col-lg-1 text-left">
                    <img src="assets/css/logo.png">
                </div>
            </div>
        </div>
        <!-- <div class="container-fluid header_gradient">
         </div> -->
    </header>
    <section id="body">
        <div class="container-fluid all-page ">
            <div class="container-fluid main-body-top" style="min-height: 426px;">
                <div class="container-fluid header1">
                    <div class="container header2">
                        <div class="row">
                            <div class="col col-lg-8">
                                <div class="col col-lg-12 text-right">
                                    <h4 class="title_">درمانگاه های تخصصی و فوق تخصصی بیمارستان دکتر شریعتی</h4>
                                </div>
                            </div>
                            <div class="col col-lg-4 text-left">
                                <a href="index.php" style="margin-top: 18px" class="btn btn-white ">صفحه نخست</a>
                                <a href="#" style="margin-top: 18px" class="btn btn-white back">صفحه قبل</a>
                            </div>
                        </div>
                    </div>
                </div>

                <ul class="breadcrumb">
                </ul>
                <div class="container-fluid main-body_" style="min-height: 426px;">
                    <div class="container content text-center">


                        <div class="row">

                            <div class="col col-lg-4"></div>
                            <div class="col col-lg-4 col-sm-12">
                                <div class="panel panel-default">
                                    <div id="print_div">
                                        <div class="panel-heading" style="text-align:center">
                                            به بیمارستان ما خوش آمدید
                                        </div>
                                        <div class="panel-body text-right" style="text-align:right">
                                            <p>
                                                <span>نام پزشک:
                                                </span><strong><?php echo $_SESSION['doctor_name']; ?></strong>
                                            </p>
                                            <p>
                                                <span>نام کلینیک :
                                                </span><strong><?php echo $_SESSION['parent_name']; ?></strong>
                                            </p>

                                            <p>
                                                <span>نام درمانگاه :
                                                </span><strong><?php echo $_SESSION['hospital_name']; ?></strong>
                                            </p>


                                            <p>
                                                <span> تاریخ : </span><strong><?php echo show_day_by_id(2) . ' - ' . $_GET['time']; ?></strong>
                                            </p>

                                            <p>
                                                <span> نوبت : </span><strong><?php echo $turn; ?></strong>
                                            </p>
                                            <?php 
                if(!isset($_SESSION['doctor_name']) || !isset($_SESSION['parent_name']) || !isset($_SESSION['hospital_name']) || !isset($_SESSION['code']) || !isset($_SESSION['day'])){
                    // header("Location: index.php");
                }
                    // unset($_SESSION['doctor_name']);
                    // unset($_SESSION['parent_name']);
                    // unset($_SESSION['hospital_name']);
                    // unset($_SESSION['code']);
                    // unset($_SESSION['day']);


                ?>


                                            <hr>

                                            <p class="text-left" style="padding-left:10px">

                                                <a href="index.php" class="btn btn-info">رفتن به صفحه اصلی</a>
                                            </p>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="col col-lg-4"></div>


                        </div>
                    </div>
                </div>
                <div class="container-fluid footer">
                    <div class="container">
                        <h4 class="company">

                        </h4>
                        <p>ساخته شده توسط : سحر تیموری</p>
                    </div>
                </div>
            </div>

    </section>
    <!-- <script src="assets/js/jquery.js"></script> -->

    <!-- <script src="assets/js/bootstrap.js"></script> -->


    <script>
        var WindowHeight = $(document).height() - 1;
        var HeaderHeight = $('.header0').innerHeight() + $('.header_gradient').innerHeight() + $('.header1')
            .innerHeight();
        var footerHeight = $('.footer').innerHeight();
        var bodyHeight = WindowHeight - HeaderHeight - footerHeight;
        $('.main-body_').css('min-height', bodyHeight);
        $('.main-body-top').css('min-height', bodyHeight);
        $('.back').click(function () {
            history.back();
            return false;
            // history.go(-1)
        })
    </script>



</body>

</html>